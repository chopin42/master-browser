# Master-browser: Sync-less password manager in your browser

This script is a sync-less password manager right in your browser. 
Based on [masterpassword.app](https://masterpassword.app). **Not compatible with masterpassword** because I didn't succeeded to reproduce the algorythm.

## How to setup

1. Install a Userscript manager like [ViolentMonkey](https://violentmonkey.github.io/)
2. Go on the following link: https://greasyfork.org/en/scripts/404231-master-browser
3. Click on 'Install script' 
4. Change the variable 'username' with your email, username or name. This thing is used to generate password so it shouldn't change over time. But it is also used to fill forms. My recommendation is to use your email address: often used in forms, doesn't change over time.
5. Change the salt with random stuff like : "edafeyuzabtcrtuygeru123456776753"
6. Click on 'confirm and install'

## How to use

This is what you can do when you encounter a login form:

1. Press ALT+J when you need to paste your email address / username
2. Write your master password in the password field
3. Press ALT+G. It will changes your master password into a unique hash based on the website's url
4. Fill the other informations of the login form
5. Confirm the form and login


1. Press ALT+J to copy your email adress
2. Press CTRL+V to paste your email adress in the right place
3. Go into the password field and write your **master password**
4. Press ALT + G to copy the master password
5. Press CTRL + V to paste your master password

> If you used this tool before and your password doesn't work anymore, please use ALT+SHIFT+G instead.

## How to contribute

Even if you can't code you can still contribute to this project! These are a few ideas that are really appreciated:

* Reporting bugs in issues
* Answering to existing issues
* Proposing new features in issues
* Making proposed features or solving bugs by pull requests
* Improving documentation and instructions
* Translate the documentation in your language
* Promote the app around you
* **If you know any security breaches, large or small. Create an issue immediately please! Very important I am not an expert!**

These are the conditions I would like you respect while doing them:

* Have respect in the issues, no insults, or off-topic comments
* Try to comment your code and explain what you did
* Check if the issue you want to post doesn't exist already, if so, please, don't create a new one
* When you post an idea or a problem you have don't write 'I have a problem with the install, can you help' but explain what browser you're using, what happened, what error code you had, what is your userscript manager, etc.
* When you post an issue, try to keep an eye on it, don't just post it and never answer to replies.

## License

This project is release under GNU-GPLv3 by SnowCode. 
